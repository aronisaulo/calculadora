package com.calculadora;



import java.util.List;


public class Operacao {
    public static int soma(int num1 , int num2){
        int soma = num1 + num2;
        return soma;
    }
    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            resultado += numero;
        }
        return  resultado;
    }

    public static int subtrair(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if(resultado==0) resultado = numero;
            else
            resultado -= numero;
        }
        return  resultado;
    }
    public static int multiplicao(List<Integer> numeros){
        int resultado = 1;
        for (Integer numero : numeros){
           resultado *= numero;
        }
        return  resultado;
    }
    public static boolean divisao(int num1 , int num2)  {
        boolean resultado = false;
        if (num1>0 && num2 >0)
            resultado =true;

        //resultado = num1 / num2;

      return resultado;


    }
    public static double divisao2(int num1 , int num2)  {
        double resultado = 0;

        try {
          resultado = num1 / num2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;


    }
}
