package com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros),6);
    }
    @Test
    public void testarOperacaoDoisNumero(){
        Assertions.assertEquals(Operacao.soma(1,2),3);
    }

    @Test
    public void testarOperacaoDeSubtrair(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(11);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.subtrair(numeros),6);
    }
    @Test
    public void testarOperacaoDeMultiplicacao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.multiplicao(numeros),6);
    }
    @Test
    public void testarOperacaoDeDivisao()  {

        Assertions.assertEquals(Operacao.divisao(12,2),true);
    }
    @Test
    public void testarOperacaoDeDivisaoPorZero()  {
        Assertions.assertEquals(Operacao.divisao(0,1), false);

    }
}
